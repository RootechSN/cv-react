import React from 'react';
import PropTypes from 'prop-types';
import Social from './Social';

import Rootech from '../images/rootech.jpg';

const About = ({avatar, name, profession, bio, email, phone, address, social}) => {
  return(
    <div className='bio'>
      <div className='avatar'>
        <img src={ Rootech } alt={ name } />
      </div>
      <div className='title'>
        <h1>{name}</h1>
        <h2>{profession}</h2>
      </div>
      <div className='desc'>
        <p>{bio}</p>
      </div>
      <div className='email'>
        <p>{email}</p>
      </div>
      <div className='phone'>
        <p>{phone}</p>
      </div>
      <div className='address'>
        <p>{address}</p>
      </div>
      <Social social={social} />
    </div>
  );
};

About.PropTypes = {
  avatar: PropTypes.string,
  name: PropTypes.string,
  profession: PropTypes.string,
  bio: PropTypes.string,
  email: PropTypes.email,
  phone: PropTypes.phone,
  address: PropTypes.string,
  social: PropTypes.node
};

export default About;
