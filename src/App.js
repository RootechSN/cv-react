import React, { Component } from 'react';
import About from './components/About';
import Experience from './components/Experience';
import Education from './components/Education';
import Certificate from './components/Certificate';
import Skills from './components/Skills';


class App extends Component {
  render() {

    const person = {
      avatar: '{ Rootech }',
      name: 'Omar SOW',
      profession: 'Software Engineer',
      bio: '❤ Full-stack Developer - Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
      email: 'rootechsn@gmail.com',
      phone: '77 880 66 77',
      address: 'Mbour, Sénégal.',
      social: [
        {name: 'facebook', url: 'https://www.facebook.com/rootechsn/'},
        {name: 'twitter', url: 'https://twitter.com/Rootech1'},
        {name: 'github', url: 'https://github.com/RootechSN'},
        {name: 'linkedin', url: 'https://www.linkedin.com/in/rootech-sn-48417515b/'}
      ],
      experience: [
        {jobTitle: 'FrontEnd', company: 'Volkeno', startDate: 'Avril 2021', endDate: 'En cours', jobDescription: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'},
        {jobTitle: 'Backend', company: 'Bakeli', startDate: 'Décembre 2019', endDate: 'Avril 2020', jobDescription: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'},
      ],
      education: [
        {degree: 'Développeur web', institution: 'Bakeli', startDate: '2019', endDate: 'En cours', description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'},
        {degree: 'Multimédia', institution: 'UVS', startDate: 'Octobre 2017', endDate: 'Decenber 2020', description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'},
      ],
      certificate: [
        {name: 'FrontEnd Developer', institution: 'Bakeli', date: 'Avril 2021', description: 'Aenean commodo ligula eget dolor. Aenean massa.' },
        {name: 'Backend Developer', institution: 'Bakeli', date: 'Décembre 2019', description: 'Aenean commodo ligula eget dolor. Aenean massa.' }
      ],
      skills: [
        {name: 'HTML5', percentage: '95%'},
        {name: 'CSS', percentage: '90%'},
        {name: 'JavaScript', percentage: '75%'},
        {name: 'PHP', percentage: '70%'},
        {name: 'Laravel', percentage: '65%'},
        {name: 'Jquery', percentage: '47%'},
        {name: 'Reactjs', percentage: '45%'},
        {name: 'Wordpress', percentage: '80%'}
      ]
    };

    return (
      <header>
        <div className='wrapper'>
          <div className='sidebar'>
            <About
              avatar={person.avatar}
              name={person.name}
              profession={person.profession}
              bio={person.bio}
              email={person.email}
              phone={person.phone}
              address={person.address}
              social={person.social} />
          </div>

          <div className='content-wrapper'>
              <div className='content'>
                <Experience experience={person.experience} />
                <Education education={person.education} />
                <Certificate certificate={person.certificate} />
                <Skills skills={person.skills} />
              </div>
          </div>

        </div>
      </header>
    );
  }
}

export default App;
